import { createRouter, createWebHistory, type RouteRecordRaw } from "vue-router";
import LoadingScreen from '@/components/LoadingScreen.vue'
import GetStarted from '@/components/GetStarted.vue'
import ModeChoice from "@/components/ModeChoice.vue";
import ConnectionChoice from "@/components/ConnectionChoice.vue";
import Login from "@/components/Login.vue";
import Register from "@/components/Register.vue";

const routes: RouteRecordRaw[] = [
    {
        path: '/',
        name: 'Loading',
        component: LoadingScreen
    },
    {
        path: '/getStarted',
        name: 'GetStarted',
        component: GetStarted
    },
    {
        path: '/modeChoice',
        name: 'ModeChoice',
        component: ModeChoice
    },
    {
        path: '/connectionChoice',
        name: 'ConnectionChoice',
        component: ConnectionChoice
    },
    {
        path: '/login',
        name: 'Login',
        component: Login
    },
    {
        path: '/register',
        name: 'Register',
        component: Register
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;